FROM python:slim-buster

LABEL PROJECT_REPO_URL         = "git@gitlab.com:jyma1991/gitlab-skyline.git" \
      PROJECT_REPO_BROWSER_URL = "https://gitlab.com/jyma1991/gitlab-skyline.git" \
      DESCRIPTION              = "GitLab Skylines"

WORKDIR /app
ADD requirements.txt requirements.txt
RUN apt update && apt upgrade -y && apt install -y python3 openscad gcc
RUN pip install -r /app/requirements.txt

ADD skyline /app/skyline
ADD gitlab.svg /app/gitlab.svg

WORKDIR /data
VOLUME /data
ENTRYPOINT ["/app/skyline"]
CMD ["--help"]
