# solidpython~=1.0.5
# aiohttp~=3.7.3
# asyncio~=3.4.3
# beautifulsoup4~=4.8.2
aiohttp==3.7.4.post0
async-timeout==3.0.1
asyncio==3.4.3
attrs==22.2.0
beautifulsoup4==4.8.2
chardet==4.0.0
euclid3==0.1
idna==3.4
multidict==6.0.4
numpy==1.24.2
pandas==1.5.3
prettytable==0.7.2
pypng==0.0.19
python-dateutil==2.8.2
pytz==2022.7.1
regex==2019.12.20
six==1.16.0
solidpython==1.0.5
soupsieve==2.3.2.post1
typing_extensions==4.4.0
yarl==1.8.2
